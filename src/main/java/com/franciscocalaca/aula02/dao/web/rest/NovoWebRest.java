package com.franciscocalaca.aula02.dao.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/novo")
public class NovoWebRest {

	@GetMapping("/{param}")
	@ApiOperation(value = "endpoint que retorna uma string concatenada")	
	public String get(String param) {
		return "OK";
	}
	
}
